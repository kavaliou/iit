from parsing import Parser
from tags import LinkTag, Text


def parse(text):
    parser = Parser()
    parser.parse(text)
    # parser.print_tree()
    return parser.stack[0]


def get_all_links(node):
    return get_all_tags_by_type(node, LinkTag)


def get_all_text_tags(node):
    return get_all_tags_by_type(node, Text)


def get_all_tags_by_type(node, tag_type):
    links = []
    for child in node.children:
        links.extend(get_all_tags_by_type(child, tag_type))
    if isinstance(node.tag, tag_type):
        links.append(node)
    return links
