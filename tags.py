class Tag(object):
    tag_name = None
    is_leaf = False

    def __init__(self, **kwargs):
        self.kwargs = kwargs
        # print(self)

    def __str__(self):
        return '<<%s, %s>>' % (self.tag_name, self.kwargs)

    def __repr__(self):
        return str(self)

    @property
    def value(self):
        return None


class Main(Tag):
    tag_name = 'main'


class Block(Tag):
    tag_name = 'block'


class BoldText(Tag):
    tag_name = 'bold'


class LinkTag(Tag):
    tag_name = 'link'


class ParagraphTag(Tag):
    tag_name = 'paragraph'


class ItalicTag(Tag):
    tag_name = 'pasta'


class EmphasizedTag(Tag):
    tag_name = 'emphasize'


class RedTextTag(Tag):
    tag_name = 'makemered'


class BoldYellowTextTag(Tag):
    tag_name = 'bye'


class AbbreviationTag(Tag):
    tag_name = 'abracadabra'


class ButtonTag(Tag):
    tag_name = 'knopka'


class ImageTag(Tag):
    tag_name = 'pic'


class Text(Tag):
    tag_name = 'text'
    is_leaf = True

    @property
    def value(self):
        return self.kwargs['text']


class TagRegistry(object):
    def __init__(self):
        self.tags = {}

    def register(self, tag):
        self.tags[tag.tag_name] = tag

    def default_tags(self):
        tags = [
            Block, BoldText, LinkTag, ParagraphTag, ItalicTag, AbbreviationTag, ButtonTag,
            EmphasizedTag, RedTextTag, BoldYellowTextTag, ImageTag,
        ]
        for tag in tags:
            self.register(tag)
