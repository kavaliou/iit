import collections
import re
from sqlite3 import dbapi2 as sqlite

from utils import parse, get_all_links, get_all_text_tags


class Crawler(object):
    def __init__(self, dbname):
        self.con = sqlite.connect(dbname)

    def __del__(self):
        self.con.close()

    def dbcommit(self):
        self.con.commit()

    def get_entry_id(self, table, field, value, create_new=True):
        cur = self.con.execute("select rowid from %s where %s='%s'" % (table, field, value))
        res = cur.fetchone()
        if res is None:
            cur = self.con.execute("insert into %s (%s) values ('%s')" % (table, field, value))
            return cur.lastrowid
        else:
            return res[0]

    def add_to_index(self, url, node):
        if self.is_indexed(url):
            return
        print('Indexing ' + url)
        text = self.get_text_only(node)
        words = self.separate_words(text)
        url_id = self.get_entry_id('urllist', 'url', url)
        for i in range(len(words)):
            word = words[i]
            word_id = self.get_entry_id('wordlist', 'word', word)
            self.con.execute("insert into wordlocation(urlid,wordid,location) values (%d,%d,%d)" % (url_id, word_id, i))

    def get_text_only(self, node):
        result = ''
        text_nodes = get_all_text_tags(node)
        for text_node in text_nodes:
            result += text_node.tag.kwargs['text'] + '\n'
        return result

    def separate_words(self, text):
        splitter = re.compile('\\W+')
        return [s.lower() for s in splitter.split(text) if s != '']

    def is_indexed(self, url):
        u = self.con.execute("select rowid from urllist where url='%s'" % url).fetchone()
        if u is not None:
            v = self.con.execute('select * from wordlocation where urlid=%d' % u[0]).fetchone()
            if v is not None:
                return True
        return False

    def add_link_ref(self, url_from, url_to, link_text):
        print(link_text)
        pass

    def crawl(self, pages, depth=2):
        for i in range(depth):
            new_pages = set()
            for page in pages:
                try:
                    with open('files/%s' % page) as text_file:
                        node = parse(text_file.read())
                except:
                    continue
                self.add_to_index(page, node)

                links_nodes = get_all_links(node)
                for link_node in links_nodes:
                    url = link_node.tag.kwargs['url']
                    if not self.is_indexed(url):
                        new_pages.add(url)
                    link_text = self.get_text_only(link_node)
                    self.add_link_ref(page, url, link_text)

                self.dbcommit()
            pages = new_pages

    def create_index_tables(self):
        self.con.execute('create table urllist(url)')
        self.con.execute('create table wordlist(word)')
        self.con.execute('create table wordlocation(urlid,wordid,location)')
        # self.con.execute('create table link(fromid integer,toid integer)')
        # self.con.execute('create table linkwords(wordid,linkid)')
        self.con.execute('create index wordidx on wordlist(word)')
        self.con.execute('create index urlidx on urllist(url)')
        self.con.execute('create index wordurlidx on wordlocation(wordid)')
        # self.con.execute('create index urltoidx on link(toid)')
        # self.con.execute('create index urlfromidx on link(fromid)')
        self.dbcommit()


class Searcher(object):
    def __init__(self, dbname):
        self.con = sqlite.connect(dbname)

    def __del__(self):
        self.con.close()

    def get_match_rows(self, query):
        field_list = 'w0.urlid'
        table_list = ''
        clause_list = ''
        word_ids = []

        words = query.lower().split(' ')
        table_number = 0

        for word in words:
            word_row = self.con.execute("select rowid from wordlist where word='%s'" % word).fetchone()
            if word_row is None:
                continue

            word_id = word_row[0]
            word_ids.append(word_id)
            if table_number > 0:
                table_list += ','
                clause_list += ' and '
                clause_list += 'w%d.urlid=w%d.urlid and ' % (table_number - 1, table_number)
            field_list += ',w%d.location' % table_number
            table_list += 'wordlocation w%d' % table_number
            clause_list += 'w%d.wordid=%d' % (table_number, word_id)
            table_number += 1

        full_query = 'select %s from %s where %s' % (field_list, table_list, clause_list)
        try:
            cur = self.con.execute(full_query)
            rows = [row for row in cur]
            return rows, word_ids
        except:
            return [], []


def crawl():
    page_list = ['test_0.iit']
    crawler = Crawler('searchindex.db')
    crawler.create_index_tables()
    crawler.crawl(page_list, depth=3)

    print([row for row in crawler.con.execute('select * from urllist')])
    print([row for row in crawler.con.execute('select * from wordlist')])
    print([row for row in crawler.con.execute('select * from wordlocation where wordid=1')])


def find(text):
    print('\n---------> %s' % text)
    engine = Searcher('searchindex.db')
    entries = engine.get_match_rows(text)
    url_ids = list(map(lambda x: x[0], entries[0]))
    counter = collections.Counter(url_ids)
    query = 'select rowid, url from urllist where rowid in (%s)' % ', '.join(map(str, list(counter.keys())))
    urls_dict = {url_if: url for url_if, url in engine.con.execute(query)}

    for url_id, count in counter.most_common():
        print('%s  Count: %s' % (urls_dict[url_id], count))


if __name__ == '__main__':
    # crawl()
    find('Lorem Ipsum')
    find('Lor Ipm')
    find('some text')
    find('Here is')
