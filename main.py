from parsing import Parser
from to_html import HTMLTransformer


def main(text):
    parser = Parser()
    parser.parse(text)
    parser.print_tree()

    print("\nLet's draw😂")
    to_html = HTMLTransformer(parser.stack[0])
    content = to_html.transform()
    return content


if __name__ == '__main__':
    for i in range(1):
        file_name = 'files/test_{}.iit'.format(i)
        html_file_name = '{}.html'.format(file_name.split('.')[0])
        with open(file_name) as text_file:
            html_content = main(text_file.read())
            with open(html_file_name, 'w') as output_file:
                output_file.write(html_content)
