from tags import TagRegistry, Main, Text


class Node(object):
    def __init__(self, tag, parent):
        self.tag = tag
        self.parent = parent
        self.children = []

    def add_child(self, node):
        self.children.append(node)

    def __str__(self):
        return '(Node %s)' % self.tag.tag_name

    def print_tree(self, indent=None):
        indent = indent or ''
        print('{}{}'.format(indent, self))
        for child in self.children:
            child.print_tree(indent+'--')


class Parser(object):
    TAG_START = '{'
    TAG_END = '}'

    def __init__(self):
        self.tag_registry = TagRegistry()
        self.tag_registry.default_tags()
        self.stack = [Node(Main(), None)]

    def parse(self, text):
        accumulator = ''
        prev_tag_start = None
        number = -1
        for char in text:
            number += 1
            accumulator += char
            if char == self.TAG_START:
                prev_tag_start = number
            if char == self.TAG_END:
                tag_text = accumulator[prev_tag_start+1:number]
                is_opening, tag_name, params = self.check_opening_tag(tag_text)
                if is_opening:  # Tag opens
                    self.create_text_tag(accumulator[:prev_tag_start])
                    accumulator = ''
                    number = -1

                    tag = self.tag_registry.tags[tag_name]
                    self.stack.append(Node(tag(**params), self.stack[-1]))

                elif self.check_tag_closing(tag_text):  # Tag closes
                    self.create_text_tag(accumulator[:prev_tag_start])
                    accumulator = ''
                    number = -1

                    closed_node = self.stack.pop()
                    self.stack[-1].add_child(closed_node)

        self.create_text_tag(accumulator[:])

    def check_opening_tag(self, tag_text):
        if tag_text in self.tag_registry.tags:
            return True, tag_text, {}
        elif ':' in tag_text:
            split = tag_text.split(':')
            tag_name = split[0]
            params_raw = split[1].split(',')
            params = {}
            for param in params_raw:
                split = param.split('=')
                params[split[0]] = split[1]
            return True, tag_name, params
        else:
            return False, None, None

    def add_to_current_node(self, tag):
        current_node = self.stack[-1]
        new_node = Node(tag, current_node)
        current_node.add_child(new_node)

    def check_tag_closing(self, tag_name):
        return tag_name.startswith('end') and tag_name[3:] in self.tag_registry.tags \
            and self.stack[-1].tag.tag_name == tag_name[3:]

    def create_text_tag(self, text):
        if len(text) == 0:
            return
        tag = Text(text=text)
        self.add_to_current_node(tag)

    def print_tree(self):
        self.stack[0].print_tree()
