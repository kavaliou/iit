from tags import Text, Main, BoldText, Block, LinkTag, ParagraphTag, ItalicTag, AbbreviationTag, \
    ButtonTag, EmphasizedTag, RedTextTag, BoldYellowTextTag, ImageTag


class HTMLElement(object):
    template = None
    params_map = {}

    @classmethod
    def get_params(cls, **kwargs):
        used_params = []
        for k, v in cls.params_map.items():
            if k in kwargs:
                used_params.append('{}="{}"'.format(v, kwargs[k]))
        return ' '.join(used_params)

    @classmethod
    def generate(cls, result, **kwargs):
        return cls.template.format(src=result, params=cls.get_params(**kwargs))


class BodyElement(HTMLElement):
    template = \
'''<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    {src}
</body>
</html>
'''


class TextElement(HTMLElement):
    template = '{src}'


class BoldTextElement(HTMLElement):
    template = '<b {params}>{src}</b>'


class DivElement(HTMLElement):
    template = '<div {params}>{src}</div>'


class LinkElement(HTMLElement):
    template = '<a {params}>{src}</a>'
    params_map = {
        'url': 'href',
        'target': 'target',
    }


class ParagraphElement(HTMLElement):
    template = '<p {params}>{src}</p>'


class ItalicElement(HTMLElement):
    template = '<i {params}>{src}</i>'


class EmElement(HTMLElement):
    template = '<em {params}>{src}</em>'


class RedTextElement(HTMLElement):
    template = '<span style="color: red;" {params}>{src}</span>'


class BoldYellowTextElement(HTMLElement):
    template = '<b style="color: yellow;" {params}>{src}</b>'


class AbbreviationElement(HTMLElement):
    template = '<abbr {params}>{src}</abbr>'
    params_map = {
        'how': 'title',
    }


class NoActionButtonElement(HTMLElement):
    template = '<button type="button" {params}>{src}</button>'


class ImageElement(HTMLElement):
    template = '<img {params}>'
    params_map = {
        'where': 'src',
        'desc': 'alt',
        'hohe': 'height',
        'breite': 'width',
    }


mapper = {
    Main: BodyElement,
    Text: TextElement,
    BoldText: BoldTextElement,
    Block: DivElement,
    LinkTag: LinkElement,
    ParagraphTag: ParagraphElement,
    ItalicTag: ItalicElement,
    EmphasizedTag: EmElement,
    RedTextTag: RedTextElement,
    BoldYellowTextTag: BoldYellowTextElement,
    AbbreviationTag: AbbreviationElement,
    ButtonTag: NoActionButtonElement,
    ImageTag: ImageElement,
}


class HTMLTransformer(object):
    def __init__(self, main_node):
        self.main_node = main_node

    def transform(self, **kwargs):
        def run(elem):
            if elem.tag.is_leaf:
                result = elem.tag.value
            else:
                result = ''
                for child in elem.children:
                    result += run(child)
            current_params = dict(kwargs, **elem.tag.kwargs)
            result = mapper[type(elem.tag)].generate(result, **current_params)
            return result

        return run(self.main_node)
