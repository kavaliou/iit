from collections import defaultdict

from utils import get_all_links, parse


def main():
    nodes = []
    for i in range(4):
        file_name = 'test_{}.iit'.format(i)
        with open('files/' + file_name) as text_file:
            main_node = parse(text_file.read())
            nodes.append((file_name, main_node))

    graph = build_graph(nodes)
    gr = {k: list(v) for k, v in graph.items()}

    for i in range(4):
        for j in range(4):
            if i == j:
                continue
            name_a = 'test_{}.iit'.format(i)
            name_b = 'test_{}.iit'.format(j)
            paths = find_all_paths(gr, name_a, name_b)
            print('{} -> {} : {}'.format(name_a, name_b, max(map(len, paths)) - 1))


def find_all_paths(graph, start, end, path=None):
    path = (path or []) + [start]
    if start == end:
        return [path]
    if start not in graph:
        return []
    paths = []
    for node in graph[start]:
        if node not in path:
            new_paths = find_all_paths(graph, node, end, path)
            paths.extend(new_paths)
    return paths


def build_graph(nodes):
    graph = defaultdict(set)
    for name, node in nodes:
        links_nodes = get_all_links(node)
        for link_node in links_nodes:
            graph[name].add(link_node.tag.kwargs['url'])
    return graph


if __name__ == '__main__':
    main()
